#include "FontTypes.h"

// Font data for Arial 8pt
extern const uint8_t PROGMEM arial_8ptBitmaps [];
extern const FONT_INFO arial_8ptFontInfo;
extern const FONT_CHAR_INFO PROGMEM arial_8ptDescriptors[];

// Font data for Arial 7pt
extern const uint8_t PROGMEM arial_7ptBitmaps [];
extern const FONT_INFO arial_7ptFontInfo;
extern const FONT_CHAR_INFO PROGMEM arial_7ptDescriptors[];

