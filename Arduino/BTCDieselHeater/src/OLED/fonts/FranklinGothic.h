#include "FontTypes.h"

// Font data for Franklin Gothic Medium Cond 8pt
extern const uint8_t PROGMEM franklinGothicMediumCond_8ptBitmaps [] ;
extern const FONT_CHAR_INFO PROGMEM franklinGothicMediumCond_8ptDescriptors[] ;
extern const FONT_INFO franklinGothicMediumCond_8ptFontInfo;

// Font data for Franklin Gothic Medium Cond 7pt
extern const uint8_t PROGMEM franklinGothicMediumCond_7ptBitmaps [];
extern const FONT_INFO franklinGothicMediumCond_7ptFontInfo;
extern const FONT_CHAR_INFO PROGMEM franklinGothicMediumCond_7ptDescriptors[];



